# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/compare/v1.2.1...v2.0.0) (2022-02-21)


### ⚠ BREAKING CHANGES

* **component-name:** `baz` prop is now deprecated

### Features

* **component-name:** removed baz prop - see BT-0000 ([3f2c283](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/commit/3f2c28343cbffd9bc359809a5e6e0ae497908e2f))

### [1.2.1](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/compare/v1.2.0...v1.2.1) (2022-02-21)


### Bug Fixes

* **component-name:** changed order of output - see BT-0000 ([d6ca72e](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/commit/d6ca72ed55fdea39313ed0267deacf342800d67e))

## [1.2.0](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/compare/v1.1.0...v1.2.0) (2022-02-21)


### Features

* **component-name:** add bar prop - see BT-0000 ([92873f1](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/commit/92873f1d307d6dfbb0a2a0362c8c9b7649dbe6ba))

## [1.1.0](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/compare/v1.0.0...v1.1.0) (2022-02-21)


### Features

* **publish:** add npm publish step - see BT-0000 ([50beaab](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/commit/50beaab1dfc3e54d3512a64f980a6bedd535ce1f))

## 1.0.0 (2022-02-20)


### Features

* **app:** added create react app - see BT-0000 ([7b0cf2c](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/commit/7b0cf2c21d38950784cc88b77f8f6b735a973b1f))
* **release:** added conventional-gitlab-releaser to pipeline - see BT-0000 ([e7dc13f](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/commit/e7dc13f9d0a0e3c1006106b01581f35d0fd109cc))
* **storybook:** added storybook to project- see BT-0000 ([de03d14](https://gitlab.com/AlvaroMontero/semantic-release-gitlab/commit/de03d14b18c230857e8b5683713c58c04d8c1d3e))
