module.exports = {
  "stories": [
    '../src/Introduction.stories.mdx', // default page
    '../src/**/*.stories.@(js|jsx|ts|tsx|mdx)'
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/preset-create-react-app"
  ],
  "framework": "@storybook/react",
  "core": {
    "builder": "webpack5"
  }
}