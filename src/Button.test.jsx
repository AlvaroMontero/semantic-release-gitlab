import { render, screen } from '@testing-library/react';
import { Button } from './Button';

test('renders', () => {
  render(<Button label="Chick Me" />);
  const element = screen.getByText(/Chick Me/i);
  expect(element).toBeInTheDocument();
});
